import java.util.Date;

public class Cumparator {
    private int id;
    private int timp_sosire;
    private int timp_plecare;

    public Cumparator(int id, int timp_sosire, int timp_plecare) {
        this.id = id;
        this.timp_sosire = timp_sosire;
        this.timp_plecare = timp_plecare;
    }

    public int getId() {
        return id;
    }

    public int getTimp_sosire() {
        return timp_sosire;
    }

    public int getTimp_plecare() {
        return timp_plecare;
    }

    @Override
    public String toString() {
        return "Cumparator{" +
                "id=" + id +
                ", timp_sosire=" + timp_sosire +
                ", timp_plecare=" + timp_plecare +
                '}';
    }
}
