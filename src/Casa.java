
import java.util.ArrayList;
import java.util.List;

public class Casa extends Thread {
    private List<Cumparator> cumparatori = new ArrayList<>();

    public Casa(){};

    public void run(){
        try{
            while(true){


                Cumparator c  = stergeCumparator();
                System.out.println("Cumparatorul " + c.getId() +" cu timpul de plecare " +c.getTimp_plecare() +" a fost sters");
                sleep((int)Math.random()*1000 + 2000);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void adaugaCumparator (Cumparator c) throws  Exception{

                cumparatori.add(c);

                notifyAll();


    }

    public synchronized Cumparator stergeCumparator () throws Exception {
        while(cumparatori.size()==0){
            wait();
        }Cumparator c=cumparatori.get(0);

                cumparatori.remove(c);
                notifyAll();

        return c;
    }

    public synchronized Integer lungimeCoada() throws Exception{
        notifyAll();
        return cumparatori.size();
    }

    public List<Cumparator> getCumparatori(){
        return cumparatori;
    }

}
