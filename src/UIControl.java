

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;


import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class UIControl implements Initializable {

    @FXML
    private Label LabelCoada1;
    @FXML
    private Label LabelNrCoada1;
    @FXML
    private Label LabelCoada2;
    @FXML
    private Label LabelNrCoada2;
    @FXML
    private Label LabelCoada3;
    @FXML
    private Label LabelNrCoada3;
    @FXML
    private TextArea Log;


    public UIControl() {
    }

    private PrintStream ps ;


    public class Console extends OutputStream {
        private TextArea Log;

        public Console(TextArea console) {
            this.Log = console;
        }

        public void appendText(String valueOf) {
            Platform.runLater(() -> Log.appendText(valueOf));
        }

        public void write(int b) throws IOException {
            appendText(String.valueOf((char)b));
        }
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ps = new PrintStream(new Console(Log));
        System.setOut(ps);
        System.setErr(ps);


        int i;
        int nr = 3;
        List<Casa> casee = new ArrayList<Casa>();
        for (int j = 0; j < nr; j++) {
            casee.add(new Casa());
        }
        for (i = 0; i < casee.size(); i++) {

            casee.get(i).start();
        }

        Magazin m = new Magazin(nr, casee);
        m.start();



        Task task = new Task<Void>() {
            @Override
            public Void call() throws Exception {

                while (true) {

                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                LabelNrCoada1.setText("Numar clienti: " + casee.get(0).lungimeCoada().toString());
                                LabelNrCoada2.setText("Numar clienti: " + casee.get(1).lungimeCoada().toString());
                                LabelNrCoada3.setText("Numar clienti: " + casee.get(2).lungimeCoada().toString());
                                String lab1 = new String();
                                String lab2 = new String();
                                String lab3 = new String();
                                for(Cumparator c : casee.get(0).getCumparatori()){
                                    lab1 += c.getId() + " ";
                                }
                                for(Cumparator c : casee.get(1).getCumparatori()){
                                    lab2 += c.getId() + " ";
                                }
                                for(Cumparator c : casee.get(2).getCumparatori()){
                                    lab3 += c.getId() + " ";
                                }
                                LabelCoada1.setText(lab1);
                                LabelCoada2.setText(lab2);
                                LabelCoada3.setText(lab3);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    Thread.sleep(100);
                }
            }
        };
        Thread th = new Thread(task);
        th.setDaemon(true);
        th.start();
    }
}
