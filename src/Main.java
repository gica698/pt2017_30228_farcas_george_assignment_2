import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;



public class Main extends Application {
    public static void main( String a[] ){

        launch(a);
    }

    @Override
    public void start(Stage stage) throws Exception{
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(Main.class.getResource("ui.fxml"));
        AnchorPane anchorPane=new AnchorPane();
        anchorPane= loader.load();
       //nchorPane.getChildren().add(new Circle(100));
        Scene scene= new Scene(anchorPane);
        stage.setScene(scene);
        stage.setTitle("Proiect");
        stage.show();
    }
}
